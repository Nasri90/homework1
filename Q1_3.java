import java.util.Scanner;
public class Q1_3 {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		
		int number=sc.nextInt();
		int rev_num=0;
		
		do {
			int res=number%10;
			number/=10;
			rev_num=rev_num*10+res;
		}while(number>0);
		System.out.println(rev_num);
	}

}
