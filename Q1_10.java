import java.util.Scanner;

public class Q1_10 {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		int number=sc.nextInt();
		if(number<2)
			System.out.println("Wrong number");
		for(int j=2;j<=number;j++)
			if(prime(j)==true)
				System.out.print(j+" ");
	}
	static boolean prime(int num) {
		boolean flag=true;

		for(int i=2;i<=num/2;i++) {
			if(num%i==0) {
				flag=false;
			}
		}
	
	
		return flag;
	}
}
