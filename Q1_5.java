import java.util.Scanner;
public class Q1_5 {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		String sentence=new String();
		sentence=sc.nextLine();
		int k=1;
		int max=1;
		int permute=0;
		char[] spell= new char[sentence.length()];
		spell[0]=sentence.charAt(0);
		int[] repeatation=new int[sentence.length()];
		repeatation[0]=1;
		
		outer: for(int i=1;i<sentence.length();i++) {
			inner: for(int j=0;j<k;j++) {
			if(sentence.charAt(i)==spell[j]) {
				repeatation[j]++;
				if (repeatation[j]>max) {
					max=repeatation[j];
				 permute=j;
				}
				
				continue outer;
			}
			}
				spell[k]=sentence.charAt(i);
				repeatation[k]=1;
				k++;
				
			}
		

		System.out.print("[");
		for(int a=0;a<k;a++) {
			System.out.print("\""+spell[a]+"\"");
		}
		System.out.println("]");
		System.out.print("[");
		for(int b=0;b<k;b++) {
			System.out.print("\""+ repeatation[b]+"\"");
		}
		System.out.println("]");
		String replaced=sentence.replace(spell[permute], '*');
		System.out.println(replaced);
}
}
